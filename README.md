# PAPI library basic example

To test the effect of thread preemptions using PAPI, run:

```bash
taskset -c 1 ./main
```

And you will see the output of three counters. Then, in another terminal run:

```bash
taskset -c 1 ./annoyer
```

And you will be able to see the variation in termina one! (I didn't see any :))
