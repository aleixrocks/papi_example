PAPI_HOME=/home/aleix/tools/papi/install
PAPI_LIB=$(PAPI_HOME)/lib
PAPI_INC=$(PAPI_HOME)/include

LDFLAGS=-L $(PAPI_LIB) -Wl,-rpath=$(PAPI_LIB) -lpthread -lpapi
CFLAGS=-I $(PAPI_INC)

all: main annoyer

main: main.c
	gcc $(CFLAGS) $^ $(LDFLAGS) -o $@

annoyer: annoyer.c
	gcc $^ -o $@

run: main
	./main

clean:
	rm -rf main annoyer
