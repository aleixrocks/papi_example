/*
 *  Copyright (C) 2020 Barcelona Supercomputing Center (BSC)
 */

#define _GNU_SOURCE

#include <sched.h>
#include <stdio.h>
#include <pthread.h>
#include <papi.h>
#include <unistd.h>
#include <stdint.h>


//const char *event[] = {
//	"PAPI_TOT_INS",
//	"PAPI_TOT_CYC",
//	"PAPI_REF_CYC"
//};
//const int num_events = 3;


const char *event[] = {
	"PAPI_L2_DCM",
};
const int num_events = 1;


int potatoe;

void handle_error(char catapum, int ret, const char *msg, const char *papi_msg)
{
	if (catapum) {
		fprintf(stderr, "Error: %s: %s\n", msg, papi_msg);
		_exit(1);
	}
}

void glibc_error(char catapum, const char *msg)
{
	if (catapum) {
		perror(msg);
		_exit(1);
	}
}

void papi_init(void)
{
	int ret;

	/* Initialize the library */
	ret = PAPI_library_init(PAPI_VER_CURRENT);
	handle_error(
		ret != PAPI_VER_CURRENT,
		ret, " when initializing the PAPI library: ",
		PAPI_strerror(ret)
	);
	// TODO enable multiplex if too many events enabled?
	//ret = PAPI_multiplex_init();
	//handle_error(
	//	ret != PAPI_OK,
	//	ret, " when initializing PAPI library multiplex support: ",
	//	PAPI_strerror(ret)
	//);
	ret = PAPI_thread_init((unsigned long (*)(void)) (pthread_self));
	handle_error(
		ret != PAPI_OK,
		ret, " when initializing the PAPI library for threads: ",
		PAPI_strerror(ret)
	);
	ret = PAPI_set_domain(PAPI_DOM_USER);
	handle_error(
		ret != PAPI_OK,
		ret, " when setting the default PAPI domain to user only: ",
		PAPI_strerror(ret)
	);

}

int papi_thread_init()
{
	int i;
	int ret;
	int code;
	int eventSet = PAPI_NULL;

	/* Register the thread into PAPI */
	ret = PAPI_register_thread();
	handle_error(
		ret != PAPI_OK,
		ret, " when registering a new thread into PAPI",
		PAPI_strerror(ret)
	);

	/* Create an EventSet */
	ret = PAPI_create_eventset(&eventSet);
	handle_error(
		ret != PAPI_OK,
		ret, " when creating a PAPI event set",
		PAPI_strerror(ret)
	);

	for (i = 0; i < num_events; i++) {
		ret = PAPI_event_name_to_code(event[i], &code);
		handle_error(
			ret != PAPI_OK,
			ret, " when initalizing counter",
			PAPI_strerror(ret)
		);

		ret = PAPI_add_event(eventSet, code);
		handle_error(
			ret != PAPI_OK,
			ret, " when adding a PAPI event to the event set",
			PAPI_strerror(ret)
		);
	}

	/* Start counting */
	ret = PAPI_start(eventSet);
	handle_error(
		ret != PAPI_OK,
		ret, " when starting a PAPI event set",
		PAPI_strerror(ret)
	);

	return eventSet;
}

void papi_thread_fini()
{
	int ret;

	ret = PAPI_unregister_thread();
	handle_error(
		ret != PAPI_OK,
		ret, " when unregistering a PAPI thread",
		PAPI_strerror(ret)
	);
}

void comput(void)
{
	const int it = 100000000;
	int i;
	double sum = 1;

	for (i = 0; i < it; i++) {
		sum += (double)(((i*sum) + 0.3)/(double)sum)*13.14/sum;
	}

	potatoe += sum;
}

void print_buffer(int id, long long *data)
{
	int i;
	const size_t size = 512;
	size_t off;
	char line[size];

	off = 0;
	line[0] = 0;

	off += snprintf(&line[off], size - off, "%d\t\t", id);
	for (i = 0; i < num_events; i++)
		off += snprintf(&line[off], size - off, "%lu\t", (uint64_t) data[i]);
	printf("%s\n", line);
}

void papi_read(int eventSet, int id)
{
	int ret;
	long long data[num_events];

	ret = PAPI_read(eventSet, data);
	handle_error(
		ret != PAPI_OK,
		ret, " when reading a PAPI event set",
		PAPI_strerror(ret)
	);

	print_buffer(id, data);

	ret = PAPI_reset(eventSet);
	if (ret != PAPI_OK) {
		handle_error(
			1,
			ret, " when resetting a PAPI event set",
			PAPI_strerror(ret)
		);
	}
}

void print_header(void)
{
	int i;
	printf("thread id\t");
	for (i = 0; i < num_events; i++)
		printf("%s\t", event[i]);
	printf("\n");
}

void * thread(void *data)
{
	int eventSet;
	int id = *((int *) data);

	eventSet = papi_thread_init();
	while (1) {
		comput();
		papi_read(id, eventSet);
	}
	papi_thread_fini();

	pthread_exit(NULL);
}

void init_threads()
{
	// note that when running threads in hardware threads belonging to the
	// same core, papi fails to initialize the eventset, because hardware
	// counters are shared for hardware threads on some architectures

	// set thread affinity here
	int affinity[] = {0, 2};
	const int ncpu = sizeof(affinity)/sizeof(int);
	pthread_t pthread[ncpu];

	pthread_attr_t attr;
	cpu_set_t cpus;
	int ret;

	pthread_attr_init(&attr);
	CPU_ZERO(&cpus);
	for (int i = 0; i < ncpu; i++)
		CPU_SET(affinity[i], &cpus);
	pthread_attr_setaffinity_np(&attr, sizeof(cpu_set_t), &cpus);

	for (int i = 0; i < ncpu; i++) {
		ret = pthread_create(&pthread[i], NULL, thread, &affinity[i]);
		glibc_error(
			ret, "when creating pthread"
		);
	}

	for (int i = 0; i < ncpu; i++) {
		pthread_join(pthread[i], NULL);
	}
}

int main(int argc, char *argv[])
{
	papi_init();

	print_header();

	init_threads();

	return 0;
}
